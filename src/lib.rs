use std::collections::HashMap;

/// expand either the self.field or Some(field)
macro_rules! fill_field {
    ($target:ident, $field:ident, $value:expr, $self:ident) => {
        tt_if! {
            condition = [{tt_equal}]
            input = [{ $target $field }]
            true = [{
                Some($value)
            }]
            false = [{
                $self.$field
            }]
        }
    };
}

macro_rules! build_builder {
    (
        $(#[$attr:meta])*
        struct $struct_name:ident [$($state_declaration:ident,)*] {
            $($field_name:ident: $field_type:ty, [$actual_state:ident => $next_state:ident])*
            #[optional fields]
            $($field_name_optional:ident: $field_type_optional:ty,)*
        },
        [builder_name] => [$sealing_mod:ident::$builder_name:ident]
    ) => {

        use crate::builder::Builder;

        $(#[$attr])*
        pub struct $struct_name {
            $($field_name: $field_type,)*
            $($field_name_optional: $field_type_optional,)*
        }

        impl $struct_name {
            pub fn builder() -> $builder_name<builder::Init> {
                builder::$builder_name::default()
            }
        }

        mod $sealing_mod {

            use super::*;

            use std::marker::PhantomData;
            use tt_call::tt_if;
            use tt_equal::tt_equal;

            macro_rules! fill_struct {
            // self : the self ident
            // data : data put as value if the target is matched
            // target : targeted field
            // Example:
            //
            // fill_struct!(self, c, c) call
            //
            // is expanded to
            //
            //BazBuilder<T> {
            //     a: self.a,
            //     b: self.b,
            //     c: Some(c),
            //     d: self.d,
            //     state:Default::default(),
            // }
            ($self:ident, $data:expr, $target:ident) => {
                    $builder_name::<T> {
                        $($field_name: fill_field!($target, $field_name, $data, $self),)*
                        $($field_name_optional: fill_field!($target, $field_name_optional, $data, $self),)*
                        state: Default::default()
                    }
            };
            // Last parameter allows to define the new state
            // fill_struct!(self, a, a, WithB) call
            //
            // is expanded to
            //
            //BazBuilder<T> {
            //     a: Some(a),
            //     b: self.b,
            //     c: self.c,
            //     d: self.d,
            //     state:Default::default(),
            // }
            ($self:ident, $data:expr, $target:ident, $state:ty) => {

                    $builder_name::<$state> {
                        $($field_name: fill_field!($target, $field_name, $data, $self),)*
                        $($field_name_optional: fill_field!($target, $field_name_optional, $data, $self),)*
                        state: Default::default()
                    }

            };
        }

            #[derive(Default)]
            pub struct Init;

            pub struct Buildable;
            $(pub struct $state_declaration;)*

            #[derive(Default)]
            pub struct $builder_name<State> {
                $($field_name: Option<$field_type>,)*
                $($field_name_optional: Option<$field_type_optional>,)*
                state: PhantomData<State>
            }

            impl $builder_name<Buildable> {
                pub fn build(self) -> $struct_name {
                    $struct_name {
                        $($field_name: self.$field_name.unwrap(),)*
                        $($field_name_optional: self.$field_name_optional.unwrap_or_default(),)*
                    }
                }
            }

            impl<T> $builder_name<T> {
                $(
                   // Expands an optional field setter
                   // indexes : Vec<Index>,
                   // is expanded as:
                   // pub fn indexes (self, indexes: Vec<Index>) -> Builder<T> {
                   //   fill_struct!(self, indexes, indexes)
                   // }
                   pub fn $field_name_optional (self, $field_name_optional: $field_type_optional) -> $builder_name<T> {
                        fill_struct!(self, $field_name_optional, $field_name_optional)
                   }
                )*

            }

            // Expand the builder field setter
            // metadata_subspace : Subspace,    [ WithRootSubspace     =>  WithMetadataSubspace ]
            // is expanded as:
            // impl Builder<WithRootSubspace> {
            //   pub fn metadata_subspace (self, metadata_subspace: Subspace) -> Builder<WithMetadataSubspace> {
            //       fill_struct!(self, metadata_subspace, metadata_subspace, WithMetadataSubspace)
            //   }
            //}
            $(impl $builder_name<$actual_state> {
                   pub fn $field_name (self, $field_name: $field_type) -> $builder_name<$next_state> {
                        fill_struct!(self, $field_name, $field_name, $next_state)
                   }
            })*
        }

    };
}

build_builder! {
    #[derive(Debug, PartialEq)]
    struct Baz
        [
            WithA,
            WithB,
        ]
    {
        a       : u8,      [ Init   =>  WithA     ]
        b       : f32,     [ WithA  =>  WithB     ]
        e       : bool,    [ WithB  =>  Buildable ]
        #[optional fields]
        d       : HashMap<u32, bool>,
        c       : Vec<i8>,
    },
    [builder_name] => [builder::Builder]
}

#[cfg(test)]
mod tests {
    use crate::Baz;
    use std::collections::HashMap;

    #[test]
    fn basic() {
        let expected = Baz {
            a: 12,
            b: 45.5,
            c: vec![12, 78, -2],
            d: HashMap::from([(45, false)]),
            e: true,
        };

        let baz = Baz::builder()
            .a(12)
            .b(45.5)
            .e(true)
            .d(HashMap::from([(45, false)]))
            .c(vec![12, 78, -2])
            .build();
        assert_eq!(expected, baz)
    }
}
